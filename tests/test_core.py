from unittest.mock import Mock

from poll.core import Engine


class TestEngine:
    def test_update_acceptance_rates(self, question):
        new_rate = 0.2
        old_rate = 0.1
        acceptance_rates = {question: old_rate}
        Engine.update_acceptance_rates(acceptance_rates, question, new_rate)

        assert acceptance_rates == {question: new_rate}

    def test_update_acceptance_rates_rate_is_none(self, question):
        new_rate = None
        old_rate = 0.1
        acceptance_rates = {question: old_rate}
        Engine.update_acceptance_rates(acceptance_rates, question, new_rate)

        assert acceptance_rates == {question: old_rate}

    def test_get_answer(self, question):
        mocked_input = 'y'
        get_input = Mock(return_value=mocked_input)
        presenter = Mock(get_input=get_input)
        engine = Engine(presenter=presenter, registry=Mock())
        result = engine.get_answer(question)

        assert result == question.to_representation(mocked_input)
        get_input.assert_called_once()

    def test_get_answerx(self, question):
        mocked_input = 'y'
        get_input = Mock(return_value=mocked_input)
        display = Mock()
        presenter = Mock(get_input=get_input, display=display)
        engine = Engine(presenter=presenter, registry=Mock())
        result = engine.get_answer(question)

        assert result == question.to_representation(mocked_input)
