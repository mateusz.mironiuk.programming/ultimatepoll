from unittest.mock import Mock

import pandas as pd
import pytest

from poll.core import Engine


class TestRegistry:
    def test_update(self, registry_with_path):
        registry, path = registry_with_path
        data = {'col1': 1, 'col2': 1, 'col3': 1}
        registry.update(data)
        col_names = list(data.keys())

        df1 = pd.read_csv(path)
        df2 = pd.DataFrame(columns=col_names, data=[data])

        assert df1.equals(df2)

    def test__contains_(self, registry_with_path):
        registry, path = registry_with_path
        assert 'col1' in registry
        assert 'not_column' not in registry

    @pytest.mark.parametrize('name', ['col1', 'col2'])
    def test_get_acceptance_rate(self, registry_with_path, name):
        registry, path = registry_with_path
        registry.update([
            {'col1': 'T', 'col2': 'T', 'col3': 'T'},
            {'col1': 'F', 'col2': 'F', 'col3': 'F'}
        ])
        assert registry.get_acceptance_rate(name) == 0.5


class TestEngine:
    def test_update_acceptance_rates(self, question):
        old_rate = 0.5
        acceptance_rates = {question: old_rate}
        new_rate = 0.1

        assert acceptance_rates[question] != new_rate

        Engine.update_acceptance_rates(
            acceptance_rates=acceptance_rates,
            question=question,
            rate=new_rate
        )

        assert acceptance_rates == {question: new_rate}

    def test_update_acceptance_rates_when_None(self, question):
        old_rate = 0.5
        acceptance_rates = {question: old_rate}
        new_rate = None

        assert acceptance_rates[question] != new_rate

        Engine.update_acceptance_rates(
            acceptance_rates=acceptance_rates,
            question=question,
            rate=new_rate
        )

        assert acceptance_rates == {question: old_rate}

    def test_get_answer(self, question):
        invalid_input = 'not_valid'
        valid_input = 'y'

        assert valid_input in question.answer_mapping
        assert invalid_input not in question.answer_mapping

        mocked_get_input = Mock(side_effect=[invalid_input, valid_input])
        mocked_display = Mock()
        mocked_presenter = Mock(
            get_input=mocked_get_input, display=mocked_display
        )

        engine = Engine(registry=Mock(), presenter=mocked_presenter)
        answer = engine.get_answer(question)

        assert answer == 'T' == question.answer_mapping[valid_input]
