#!/usr/bin/env python
from setuptools import setup

with open('requirements.txt') as fd:
    require = [lib for lib in fd.read().splitlines()]

with open('requirements-testing.txt') as fd:
    tests_require = [lib for lib in fd.read().splitlines()]

setup(
    name="ultimatepoll",
    python_requires=">=3.7",
    install_requires=require,
    extras_require={
        'testing': tests_require,
    },
)
