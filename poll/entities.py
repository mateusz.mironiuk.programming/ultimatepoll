from dataclasses import dataclass
from typing import Sequence, Optional


@dataclass(frozen=True)
class Question:
    name: str
    text: str
    evaluation: dict
    answer_mapping: dict

    def is_valid(self, raw_answer):
        return raw_answer in self.answer_mapping

    def to_representation(self, raw_answer: str):
        return self.answer_mapping[raw_answer.lower()]

    def get_acceptance_rate(self, answer):
        return self.evaluation[answer]

    def __hash__(self):
        return hash(self.name)


@dataclass(frozen=True)
class Questionnaire:
    initial_text: str
    questions: Sequence[Question]
    ending_text: Optional[str]

    def __iter__(self):
        for question in self.questions:
            yield question
