import pytest

from poll.entities import Question
from poll.main import Registry

import pandas as pd


@pytest.fixture
def question():
    return Question(
        name='example_name',
        text='example text',
        evaluation={'T': 1, 'F': 0},
        answer_mapping={'y': 'T', 'n': 'F'}
    )


@pytest.fixture
def registry_with_path(tmp_path):
    directory = tmp_path / 'storage'
    directory.mkdir()
    file = directory / 'data.csv'

    df = pd.DataFrame(columns=['col1', 'col2', 'col3'])
    df.to_csv(file, index=False)

    registry = Registry(file)
    return registry, file
