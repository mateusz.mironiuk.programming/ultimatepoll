import sys

from functools import reduce, wraps, partial
from typing import Dict, Sequence, Optional

from poll.entities import Questionnaire, Question
from poll.interfaces import IRegistry, IPresenter


def catch_sys_exit(func=None, msg='Generic thanks for survey'):
    if func is None:
        return partial(catch_sys_exit, msg=msg)

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except (SystemExit, KeyboardInterrupt):
            print(f'\n{msg}')
            sys.exit(0)

    return wrapper


class Engine:
    def __init__(self, *, registry: IRegistry, presenter: IPresenter) -> None:
        self.registry = registry
        self.presenter = presenter

    @catch_sys_exit(msg="Thank you for YouGov survey")
    def start(self, questionnaire: Questionnaire):
        acceptance_rates = self.get_acceptance_rates(questionnaire)
        answers = {}

        self.presenter.display(questionnaire.initial_text)

        for question in questionnaire:
            acceptance_rate = self.calculate_acceptance_rate(
                tuple(acceptance_rates.values())
            )
            acceptance_rate_msg = self.get_acceptance_rate_msg(acceptance_rate)
            self.presenter.display(acceptance_rate_msg)
            self.presenter.display(self.get_question_msg(question))
            answer = self.get_answer(question)

            rate = question.get_acceptance_rate(answer)

            self.update_acceptance_rates(acceptance_rates, question, rate)
            answers[question] = answer

        self.registry.update(
            {question.name: answer for question, answer in answers.items()}
        )

        self.presenter.display(
            questionnaire.ending_text or
            "Thank you for  participating in Survey!"
        )

    @staticmethod
    def get_question_msg(question):
        options = "/".join(sorted(question.answer_mapping.keys()))
        return f'{question.text} ({options})'

    @staticmethod
    def update_acceptance_rates(
            acceptance_rates: Dict[Question, float],
            question: Question,
            rate: Optional[float]
    ) -> None:
        if rate is not None:
            acceptance_rates.update({question: rate})

    @staticmethod
    def get_acceptance_rate_msg(rate: float):
        return f'Acceptance rate: {round(rate * 100, 2)}%'

    @staticmethod
    def get_repeat_msg(raw_answer):
        return f'\'{raw_answer}\' is not a valid answer. Try again'

    def get_answer(self, question):
        while True:
            raw_answer = self.presenter.get_input().strip().lower()
            if raw_answer == 'q' or 'quit' in raw_answer:
                quit()
            if question.is_valid(raw_answer):
                return question.to_representation(raw_answer)
            self.presenter.display(self.get_repeat_msg(raw_answer))

    def get_acceptance_rates(self, questionnaire) -> Dict[Question, float]:
        return {
            question: self.registry.get_acceptance_rate(question.name)
            for question in questionnaire
        }

    @staticmethod
    def calculate_acceptance_rate(rates: Sequence[float]):
        if len(rates) == 1:
            return list(rates)[0]
        return reduce(lambda x, y: x * y, rates)
