# Welcome to UltimatePoll

**UltimatePoll** - world's best python-based modular polling framework.

Welcome to UltomatePoll's documentation. Get started with Installation and 
then get an overview with the Quickstart.


# Installation
### Python Version
We recommend using the latest version of Python 3. Flask supports **Python 3.7** or newer.

#### Dependencies
pandas 0.24.2  
pytest 4.6.2 (dev)  
flake8==3.7.7 (dev)  

# Quickstart
Download repository:
```
git clone git@gitlab.com:mateusz.mironiuk.programming/ultimatepoll.git
```

Move to the project directory:
```
cd ultimatepoll
```

Create virtual environment

```
python3.7 -m venv venv
```

Acquire venv

```
source ./venv/bin/activate
```


Instal dependencies

```
pip install -e .
```

## Run demo

If you want to run demo, simply type:

```
./run.sh
```


## Run tests

If you want to run tests install testing dependencies:

```
pip install -e .[testing]
```

Then run tests

```
pytest tests/
```
