import pandas as pd

from poll.core import Engine
from poll.entities import Question, Questionnaire
from poll.interfaces import IPresenter, IRegistry


class Presenter(IPresenter):
    def get_input(self) -> str:
        return input('>>> ')

    def display(self, messages):
        print(messages)


class Registry(IRegistry):
    def __init__(self, path):
        self.path = path
        self.df = pd.read_csv(path)

    def get_acceptance_rate(self, name: str) -> float:
        if len(self.df) == 0:
            return 1
        return self.df[name].value_counts().loc['F'] / len(self.df)

    def update(self, data: dict) -> None:
        self.df = self.df.append(data, ignore_index=True)
        self.df.to_csv(self.path, index=False)

    def __contains__(self, name):
        return name in self.df


if __name__ == '__main__':
    answer_mapping = {
        'yes': 'T',
        'y': 'T',
        'no': 'F',
        'n': 'F',
        's': 'U',
        'skip': 'U'
    }
    evaluation = {'F': 1, 'T': 0, 'U': None}
    questions = (
        Question(
            name='non resident',
            text='Do you live outside of UK?',
            evaluation=evaluation,
            answer_mapping=answer_mapping,
        ),
        Question(
            name='underaged',
            text='Are you underaged?',
            evaluation=evaluation,
            answer_mapping=answer_mapping,
        ),
        Question(
            name='unemployed',
            text='Are you unemployed?',
            evaluation=evaluation,
            answer_mapping=answer_mapping,
        ),
        Question(
            name='imprisoned',
            text='Have you ever been in prison?',
            evaluation=evaluation,
            answer_mapping=answer_mapping,
        ),
        Question(
            name='previous_survey',
            text='Have you previously participated in an online survey?',
            evaluation=evaluation,
            answer_mapping=answer_mapping,
        ),
    )
    questionnaire = Questionnaire(
        initial_text="Welcome in YouGov survey",
        questions=questions,
        ending_text="Thank you for the survey!",
    )

    Engine(
        registry=Registry('first_survey.csv'),
        presenter=Presenter()
    ).start(questionnaire)
