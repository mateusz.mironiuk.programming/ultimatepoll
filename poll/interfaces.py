import abc


class IRegistry(abc.ABC):
    @abc.abstractmethod
    def update(self, data: dict) -> None:
        ...

    @abc.abstractmethod
    def get_acceptance_rate(self, name: str) -> float:
        ...


class IPresenter(abc.ABC):
    @abc.abstractmethod
    def display(self, messages) -> None:
        ...

    @abc.abstractmethod
    def get_input(self) -> str:
        ...
